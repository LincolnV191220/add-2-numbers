function sum(stn1, stn2) {
    let result = ''
    let memory = 0
    let i = stn1.length - 1
    let j = stn2.length - 1

    while (i >= 0 || j >= 0) {
        let x = i >= 0 ? Number(stn1.charAt(i)) : 0;
        let y = j >= 0 ? Number(stn2.charAt(j)) : 0;

        let sum = x + y + memory

        if (sum >= 10) {
            memory = 1
            sum -= 10
        } else {
            memory = 0
        }

        console.log(`sum = ${x} + ${y}`);
        console.log(memory);
        result = sum.toString() + result

        i--;
        j--;
    }

    return result
}

console.log("Kết quả là", sum("1234", "897"));